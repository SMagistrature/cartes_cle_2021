import os

import matplotlib.pyplot as plt

from . import evol, magistrats

plot_params1 = {
    "cmap": "autumn",
    "figsize": (12, 12),
    "scheme": "user_defined",
    "legend": True,
}
plot_params1r = {
    "cmap": "autumn_r",
    "figsize": (12, 12),
    "scheme": "user_defined",
    "legend": True,
}
plot_params1g = {
    "cmap": "YlGn",
    "figsize": (12, 12),
    "scheme": "user_defined",
    "legend": True,
}
plot_params2 = {
    "cmap": "autumn",
    "scheme": "user_defined",
    "tiles": "cartoDB positron",
    "min_zoom": 6,
}
plot_params3 = {
    "cmap": "autumn_r",
    "scheme": "user_defined",
    "tiles": "cartoDB positron",
    "min_zoom": 6,
}
plot_params4 = {
    "cmap": "YlGn",
    "scheme": "user_defined",
    "tiles": "cartoDB positron",
    "min_zoom": 6,
}
public_url = os.path.join("..", "pages", "public")

divisions = [0.25, 0.5, 0.75, 0.95]


def carte_fixe(carte, titre, c, fichier, an):
    if "_population" in fichier:
        f = carte.plot(column=titre, **plot_params1r, classification_kwds=c)
    elif "_cle" in fichier:
        f = carte.plot(column=titre, **plot_params1g, classification_kwds=c)
    else:
        f = carte.plot(column=titre, **plot_params1, classification_kwds=c)
    f.set_title(titre, {"fontsize": "15", "fontweight": "3"})
    f.axis("off")
    plt.savefig(os.path.join(public_url, "resultats", an, "cartes", fichier))
    plt.close()


def cartographier(an="2024", limite=None, save=False):
    carte_france = magistrats.localisation(an=an)
    carte_tj = carte_france[0]
    carte_ca = carte_france[1]
    carte_tpe = carte_tj.dissolve(by="TPE", aggfunc="sum")

    format_population_tj = [
        "{0:,}".format(p).replace(",", " ") for p in carte_tj["POPULATION"]
    ]
    format_population_ca = [
        "{0:,}".format(p).replace(",", " ") for p in carte_ca["POPULATION"]
    ]

    default_drop_ca = [
        "Siège (CA juridiction)",
        "Siège (placés)",
        "Siège (en TJ)",
        "Parquet général",
        "Parquet (placés)",
        "Parquet (en TJ)",
        "JE",
        "JI",
        "JAP",
        "JLD",
        "JCP",
        "TPE",
    ]
    default_drop_tj = ["Siège", "Parquet", "JE", "JI", "JAP", "JLD", "JCP", "TPE"]

    html_params = {
        "tj_siege": {
            "titre": "Juges par million d‘habitants",
            "to_drop": ["Parquet", "Siège", "JE", "JI", "JAP", "JLD", "JCP", "TPE"],
            "to_insert": {
                1: (
                    "Juges par million d‘habitants",
                    round(carte_tj["Siège"] * 1e6 / carte_tj["POPULATION"], 2),
                ),
                3: ("Nombre de magistrats du siège", carte_tj["Siège"]),
                "JE par million d‘habitants": round(
                    carte_tj["JE"] * 1e6 / carte_tj["POPULATION"], 2
                ),
                "JI par million d‘habitants": round(
                    carte_tj["JI"] * 1e6 / carte_tj["POPULATION"], 2
                ),
                "JAP par million d‘habitants": round(
                    carte_tj["JAP"] * 1e6 / carte_tj["POPULATION"], 2
                ),
                "JLD par million d‘habitants": round(
                    carte_tj["JLD"] * 1e6 / carte_tj["POPULATION"], 2
                ),
                "JCP par million d‘habitants": round(
                    carte_tj["JCP"] * 1e6 / carte_tj["POPULATION"], 2
                ),
                "POPULATION": format_population_tj,
            },
        },
        "tj_parquet": {
            "titre": "Parquetiers par million d‘habitants",
            "to_drop": default_drop_tj,
            "to_insert": {
                1: (
                    "Parquetiers par million d‘habitants",
                    round(carte_tj["Parquet"] * 1e6 / carte_tj["POPULATION"], 2),
                ),
                3: ("Nombre de magistrats du parquet", carte_tj["Parquet"]),
                "POPULATION": format_population_tj,
            },
        },
        "tj_JLD": {
            "titre": "JLD par million d‘habitants",
            "to_drop": default_drop_tj,
            "to_insert": {
                1: (
                    "JLD par million d‘habitants",
                    round(carte_tj["JLD"] * 1e6 / carte_tj["POPULATION"], 2),
                ),
                3: ("Nombre de JLD", carte_tj["JLD"]),
                "POPULATION": format_population_tj,
            },
        },
        "tj_JCP": {
            "titre": "JCP par million d‘habitants",
            "to_drop": default_drop_tj,
            "to_insert": {
                1: (
                    "JCP par million d‘habitants",
                    round(carte_tj["JCP"] * 1e6 / carte_tj["POPULATION"], 2),
                ),
                3: ("Nombre de JCP", carte_tj["JCP"]),
                "POPULATION": format_population_tj,
            },
        },
        "tj_JE": {
            "titre": "Juges des enfants par million d‘habitants",
            "to_drop": ["Siège", "Parquet", "JE", "JI", "JAP", "JLD", "JCP", "TJ"],
            "to_insert": {
                1: (
                    "Juges des enfants par million d‘habitants",
                    round(carte_tpe["JE"] * 1e6 / carte_tpe["POPULATION"], 2),
                ),
                3: ("Nombre de juges des enfants", carte_tpe["JE"]),
                "POPULATION": [
                    "{0:,}".format(p).replace(",", " ") for p in carte_tpe["POPULATION"]
                ],
            },
        },
        "tj_JI": {
            "titre": "Juges d‘instruction par million d‘habitants",
            "to_drop": default_drop_tj,
            "to_insert": {
                1: (
                    "Juges d‘instruction par million d‘habitants",
                    round(carte_tj["JI"] * 1e6 / carte_tj["POPULATION"], 2),
                ),
                3: ("Nombre de juges d‘instruction", carte_tj["JI"]),
                "POPULATION": format_population_tj,
            },
        },
        "tj_JAP": {
            "titre": "Juges de l‘application des peines par million d‘habitants",
            "to_drop": default_drop_tj,
            "to_insert": {
                1: (
                    "Juges de l‘application des peines par million d‘habitants",
                    round(carte_tj["JAP"] * 1e6 / carte_tj["POPULATION"], 2),
                ),
                3: ("Nombre de juges de l‘application des peines", carte_tj["JAP"]),
                "POPULATION": format_population_tj,
            },
        },
        "tj_ensemble": {
            "titre": "Magistrats par million d‘habitants",
            "to_drop": default_drop_tj,
            "to_insert": {
                1: (
                    "Magistrats par million d‘habitants",
                    round(
                        (carte_tj["Siège"] + carte_tj["Parquet"])
                        * 1e6
                        / carte_tj["POPULATION"],
                        2,
                    ),
                ),
                3: (
                    "Nombre de magistrats du TJ",
                    carte_tj["Siège"] + carte_tj["Parquet"],
                ),
                "POPULATION": format_population_tj,
            },
        },
        "ca_siege": {
            "titre": "Magistrats du siège par million d‘habitants",
            "to_drop": [
                "Parquet général",
                "Parquet (placés)",
                "Parquet (en TJ)",
                "JE",
                "JI",
                "JAP",
                "JLD",
                "JCP",
                "TPE",
            ],
            "to_insert": {
                1: (
                    "Magistrats du siège par million d‘habitants",
                    round(
                        (
                            carte_ca["Siège (CA juridiction)"]
                            + carte_ca["Siège (placés)"]
                            + carte_ca["Siège (en TJ)"]
                        )
                        * 1e6
                        / carte_ca["POPULATION"],
                        2,
                    ),
                ),
                3: (
                    "Nombre de magistrats du siège (TJ+CA+placés)",
                    carte_ca["Siège (CA juridiction)"]
                    + carte_ca["Siège (placés)"]
                    + carte_ca["Siège (en TJ)"],
                ),
                "JE par million d‘habitants": round(
                    carte_ca["JE"] * 1e6 / carte_ca["POPULATION"], 2
                ),
                "JI par million d‘habitants": round(
                    carte_ca["JI"] * 1e6 / carte_ca["POPULATION"], 2
                ),
                "JAP par million d‘habitants": round(
                    carte_ca["JAP"] * 1e6 / carte_ca["POPULATION"], 2
                ),
                "JLD par million d‘habitants": round(
                    carte_ca["JLD"] * 1e6 / carte_ca["POPULATION"], 2
                ),
                "JCP par million d‘habitants": round(
                    carte_ca["JCP"] * 1e6 / carte_ca["POPULATION"], 2
                ),
                "Pourcentage de placés siège sur total siège": round(
                    100
                    * carte_ca["Siège (placés)"]
                    / (
                        carte_ca["Siège (CA juridiction)"]
                        + carte_ca["Siège (placés)"]
                        + carte_ca["Siège (en TJ)"]
                    ),
                    2,
                ),
                "POPULATION": format_population_ca,
            },
        },
        "ca_parquet": {
            "titre": "Magistrats du parquet par million d‘habitants",
            "to_drop": [
                "Siège (CA juridiction)",
                "Siège (placés)",
                "Siège (en TJ)",
                "JE",
                "JI",
                "JAP",
                "JLD",
                "JCP",
                "TPE",
            ],
            "to_insert": {
                1: (
                    "Magistrats du parquet par million d‘habitants",
                    round(
                        (
                            carte_ca["Parquet général"]
                            + carte_ca["Parquet (placés)"]
                            + carte_ca["Parquet (en TJ)"]
                        )
                        * 1e6
                        / carte_ca["POPULATION"],
                        2,
                    ),
                ),
                3: (
                    "Nombre de magistrats du parquet (TJ+CA+placés)",
                    carte_ca["Parquet général"]
                    + carte_ca["Parquet (placés)"]
                    + carte_ca["Parquet (en TJ)"],
                ),
                "Pourcentage de placés parquet sur total parquet": round(
                    100
                    * carte_ca["Parquet (placés)"]
                    / (
                        carte_ca["Parquet général"]
                        + carte_ca["Parquet (placés)"]
                        + carte_ca["Parquet (en TJ)"]
                    ),
                    2,
                ),
                "POPULATION": format_population_ca,
            },
        },
        "ca_JCP": {
            "titre": "JCP par million d‘habitants",
            "to_drop": default_drop_ca,
            "to_insert": {
                1: (
                    "JCP par million d‘habitants",
                    round(carte_ca["JCP"] * 1e6 / carte_ca["POPULATION"], 2),
                ),
                3: ("Nombre de JCP", carte_ca["JCP"]),
                "POPULATION": format_population_ca,
            },
        },
        "ca_JLD": {
            "titre": "JLD par million d‘habitants",
            "to_drop": default_drop_ca,
            "to_insert": {
                1: (
                    "JLD par million d‘habitants",
                    round(carte_ca["JLD"] * 1e6 / carte_ca["POPULATION"], 2),
                ),
                3: ("Nombre de JLD", carte_ca["JLD"]),
                "POPULATION": format_population_ca,
            },
        },
        "ca_JI": {
            "titre": "Juges d‘instruction par million d‘habitants",
            "to_drop": default_drop_ca,
            "to_insert": {
                1: (
                    "Juges d‘instruction par million d‘habitants",
                    round(carte_ca["JI"] * 1e6 / carte_ca["POPULATION"], 2),
                ),
                3: ("Nombre de juges d‘instruction", carte_ca["JI"]),
                "POPULATION": format_population_ca,
            },
        },
        "ca_JE": {
            "titre": "Juges des enfants par million d‘habitants",
            "to_drop": default_drop_ca,
            "to_insert": {
                1: (
                    "Juges des enfants par million d‘habitants",
                    round(carte_ca["JE"] * 1e6 / carte_ca["POPULATION"], 2),
                ),
                3: ("Nombre de juges des enfants", carte_ca["JE"]),
                "POPULATION": format_population_ca,
            },
        },
        "ca_JAP": {
            "titre": "Juges de l‘application des peines par million d‘habitants",
            "to_drop": default_drop_ca,
            "to_insert": {
                1: (
                    "Juges de l‘application des peines par million d‘habitants",
                    round(carte_ca["JAP"] * 1e6 / carte_ca["POPULATION"], 2),
                ),
                3: ("Nombre de juges de l‘application des peines", carte_ca["JAP"]),
                "POPULATION": format_population_ca,
            },
        },
        "ca_ensemble": {
            "titre": "Magistrats par million d‘habitants",
            "to_drop": default_drop_ca,
            "to_insert": {
                1: (
                    "Magistrats par million d‘habitants",
                    round(
                        (
                            carte_ca["Siège (CA juridiction)"]
                            + carte_ca["Siège (placés)"]
                            + carte_ca["Siège (en TJ)"]
                            + carte_ca["Parquet général"]
                            + carte_ca["Parquet (placés)"]
                            + carte_ca["Parquet (en TJ)"]
                        )
                        * 1e6
                        / carte_ca["POPULATION"],
                        2,
                    ),
                ),
                3: (
                    "Nombre de magistrats (TJ + cour + placés)",
                    carte_ca["Siège (CA juridiction)"]
                    + carte_ca["Siège (placés)"]
                    + carte_ca["Siège (en TJ)"]
                    + carte_ca["Parquet général"]
                    + carte_ca["Parquet (placés)"]
                    + carte_ca["Parquet (en TJ)"],
                ),
                "Pourcentage de placés (max statutaire = 6,66%)": round(
                    100
                    * (carte_ca["Siège (placés)"] + carte_ca["Parquet (placés)"])
                    / (
                        carte_ca["Siège (CA juridiction)"]
                        + carte_ca["Siège (placés)"]
                        + carte_ca["Siège (en TJ)"]
                        + carte_ca["Parquet général"]
                        + carte_ca["Parquet (placés)"]
                        + carte_ca["Parquet (en TJ)"]
                    ),
                    2,
                ),
                "POPULATION": format_population_ca,
            },
        },
        "tj_population": {
            "titre": "Population du ressort (en milliers d‘habitants)",
            "to_drop": default_drop_tj + ["POPULATION"],
            "to_insert": {
                1: (
                    "Population du ressort (en milliers d‘habitants)",
                    round(carte_tj["POPULATION"] / 1000),
                )
            },
        },
        "ca_population": {
            "titre": "Population du ressort (en milliers d‘habitants)",
            "to_drop": default_drop_ca + ["POPULATION"],
            "to_insert": {
                1: (
                    "Population du ressort (en milliers d‘habitants)",
                    round(carte_ca["POPULATION"] / 1000),
                )
            },
        },
        "tj_population_evol": {
            "titre": "Evolution de la population du ressort depuis 2021 (en milliers d‘habitants)",
            "to_drop": default_drop_tj + ["POPULATION"],
            "to_insert": {
                1: (
                    "Evolution de la population du ressort depuis 2021 (en milliers d‘habitants)",
                    round(evol.population(2021, an)[0]["evol_pop_2021"] / 1000),
                )
            },
        },
        "ca_population_evol": {
            "titre": "Evolution de la population du ressort depuis 2021 (en milliers d‘habitants)",
            "to_drop": default_drop_ca + ["POPULATION"],
            "to_insert": {
                1: (
                    "Evolution de la population du ressort depuis 2021 (en milliers d‘habitants)",
                    round(evol.population(2021, an)[1]["evol_pop_2021"] / 1000),
                )
            },
        },
        "ca_cle": {
            "titre": "Nombre de magistrats (TJ + cour + placés)",
            "to_drop": ["TPE"],
            "to_insert": {
                1: (
                    "Nombre de magistrats (TJ + cour + placés)",
                    carte_ca["Siège (CA juridiction)"]
                    + carte_ca["Siège (placés)"]
                    + carte_ca["Siège (en TJ)"]
                    + carte_ca["Parquet général"]
                    + carte_ca["Parquet (placés)"]
                    + carte_ca["Parquet (en TJ)"],
                ),
                "Pourcentage de placés (max statutaire = 6,66%)": round(
                    100
                    * (carte_ca["Siège (placés)"] + carte_ca["Parquet (placés)"])
                    / (
                        carte_ca["Siège (CA juridiction)"]
                        + carte_ca["Siège (placés)"]
                        + carte_ca["Siège (en TJ)"]
                        + carte_ca["Parquet général"]
                        + carte_ca["Parquet (placés)"]
                        + carte_ca["Parquet (en TJ)"]
                    ),
                    2,
                ),
                "POPULATION": format_population_ca,
            },
        },
        "tj_cle": {
            "titre": "Nombre de magistrats du TJ",
            "to_drop": ["TPE"],
            "to_insert": {
                1: (
                    "Nombre de magistrats du TJ",
                    carte_tj["Siège"] + carte_tj["Parquet"],
                ),
                "POPULATION": format_population_tj,
            },
        },
        "ca_cle_evol": {
            "titre": "Evolution du nb de magistrats depuis 2021",
            "to_drop": default_drop_ca + ["POPULATION"],
            "to_insert": {
                1: (
                    "Evolution du nb de magistrats depuis 2021",
                    evol.cle(2021, an)[1]["evol_Magistrats_2021"],
                ),
                2: (
                    "Evolution de la population du ressort depuis 2021 (en milliers d‘habitants)",
                    round(evol.population(2021, an)[1]["evol_pop_2021"] / 1000),
                ),
            },
        },
        "tj_cle_evol": {
            "titre": "Evolution du nb de magistrats depuis 2021",
            "to_drop": default_drop_tj + ["POPULATION"],
            "to_insert": {
                1: (
                    "Evolution du nb de magistrats depuis 2021",
                    evol.cle(2021, an)[0]["evol_Magistrats_2021"],
                ),
                2: (
                    "Evolution de la population du ressort depuis 2021 (en milliers d‘habitants)",
                    round(evol.population(2021, an)[0]["evol_pop_2021"] / 1000),
                ),
            },
        },
    }

    for carte in html_params:
        if limite and carte != limite:
            pass
        elif "_evol" in carte and an == "2021":
            pass
        else:
            if carte == "tj_JE":
                df = carte_tpe
            elif "tj_" in carte:
                df = carte_tj
            else:
                df = carte_ca

            t = html_params[carte]["titre"]

            df = df.drop(columns=html_params[carte]["to_drop"])

            for col in html_params[carte]["to_insert"]:
                if type(col) is int:
                    df.insert(
                        col,
                        html_params[carte]["to_insert"][col][0],
                        html_params[carte]["to_insert"][col][1],
                    )
                else:
                    df[col] = html_params[carte]["to_insert"][col]

            if carte == "ca_siege":
                df = df.rename(
                    columns={
                        "Siège (CA juridiction)": "dont en CA",
                        "Siège (placés)": "dont placés",
                        "Siège (en TJ)": "dont en TJ",
                    }
                )
            bins = {"bins": list(df[t].quantile(divisions))}

            if save:
                if "_population" in carte:
                    df.explore(column=t, **plot_params3, classification_kwds=bins).save(
                        f"{public_url}/resultats/{an}/cartes/{carte}.html"
                    )
                elif "_cle" in carte:
                    df.explore(column=t, **plot_params4, classification_kwds=bins).save(
                        f"{public_url}/resultats/{an}/cartes/{carte}.html"
                    )
                else:
                    df.explore(column=t, **plot_params2, classification_kwds=bins).save(
                        f"{public_url}/resultats/{an}/cartes/{carte}.html"
                    )
                carte_fixe(df, t, bins, f"{carte}.png", an)
                print(f"Carte {carte}   : ok")
            else:
                if "_population" in carte:
                    return df.explore(
                        column=t, **plot_params3, classification_kwds=bins
                    )
                elif "_cle" in carte:
                    return df.explore(
                        column=t, **plot_params4, classification_kwds=bins
                    )
                else:
                    return df.explore(
                        column=t, **plot_params2, classification_kwds=bins
                    )
