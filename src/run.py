import os

from magpop import cartes, communes, magistrats, ressorts

if __name__ == "__main__":
    a = str(input("Saisissez l'année à analyser ?........"))
    if os.path.isdir(os.path.join("sources", a)):
        print("\n- importation données IGN (cela peut prendre plusieurs minutes)")
        communes.import_ign(an=a)
        print("OK\n\n- extraction données IGN")
        communes.extract_ign(a)
        print("OK\n\n- fusion avec la base de données des ressorts")
        ressorts.perimetres(a)
        print("OK\n\n- Ecriture des synthèses Excel")
        magistrats.ecrire(a)
        print("OK\n\n- Ecriture des cartes")
        cartes.cartographier(an=a, save=True)
        print("OK")
    else:
        print(f"Les données ne sont pas (encore) disponibles pour l'année {a}")
