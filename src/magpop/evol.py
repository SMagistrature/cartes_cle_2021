import numpy as np

from . import magistrats


def population(an1, an2):
    # Retourne l'évolution de la population, pour chaque ressort, entre 2 années données,
    # ainsi que la part de cette augmentation dans l'augmentation globale
    an1, an2 = str(an1), str(an2)
    df1 = magistrats.localisation(False, an1)
    df2 = magistrats.localisation(False, an2)
    df1_tj, df1_ca, df2_tj, df2_ca = df1[0], df1[1], df2[0], df2[1]

    df2_tj.insert(2, "evol_pop_" + an1, df2_tj["POPULATION"] - df1_tj["POPULATION"])
    df2_ca.insert(2, "evol_pop_" + an1, df2_ca["POPULATION"] - df1_ca["POPULATION"])

    df2_tj.insert(
        3,
        "augm_pop_" + an1,
        np.where(df2_tj["evol_pop_" + an1] <= 0, 0, df2_tj["evol_pop_" + an1]),
    )
    df2_ca.insert(
        3,
        "augm_pop_" + an1,
        np.where(df2_ca["evol_pop_" + an1] <= 0, 0, df2_ca["evol_pop_" + an1]),
    )
    augm_totale_ca = df2_ca["augm_pop_" + an1].sum()
    augm_totale_tj = df2_tj["augm_pop_" + an1].sum()

    df2_tj.insert(
        4, "part_augm_totale_" + an1, df2_tj["augm_pop_" + an1] / augm_totale_tj
    )
    df2_ca.insert(
        4, "part_augm_totale_" + an1, df2_ca["augm_pop_" + an1] / augm_totale_ca
    ),
    df2_tj = df2_tj.drop(
        columns=["TPE", "Siège", "Parquet", "JE", "JI", "JAP", "JCP", "JLD"]
    )
    df2_ca = df2_ca.drop(
        columns=[
            "TPE",
            "Siège (CA juridiction)",
            "Siège (placés)",
            "Siège (en TJ)",
            "Parquet général",
            "Parquet (placés)",
            "Parquet (en TJ)",
            "JE",
            "JI",
            "JAP",
            "JCP",
            "JLD",
        ]
    )
    return [df2_tj, df2_ca, df1_tj, df1_ca]


def cle(an1, an2):
    # Retourne l'évolution de la CLE, pour chaque ressort, entre 2 années données,
    # ainsi que la part de cette augmentation dans l'augmentation globale des postes
    an1, an2 = str(an1), str(an2)
    df1 = magistrats.localisation(False, str(an1))
    df2 = magistrats.localisation(False, str(an2))
    df1_tj, df1_ca, df2_tj, df2_ca = df1[0], df1[1], df2[0], df2[1]
    df1_tj.insert(2, "Magistrats", df1_tj["Siège"] + df1_tj["Parquet"])
    df2_tj.insert(2, "Magistrats", df2_tj["Siège"] + df2_tj["Parquet"])
    df1_ca.insert(
        2,
        "Magistrats",
        df1_ca["Siège (CA juridiction)"]
        + df1_ca["Siège (placés)"]
        + df1_ca["Siège (en TJ)"]
        + df1_ca["Parquet général"]
        + df1_ca["Parquet (placés)"]
        + df1_ca["Parquet (en TJ)"],
    )
    df2_ca.insert(
        2,
        "Magistrats",
        df2_ca["Siège (CA juridiction)"]
        + df2_ca["Siège (placés)"]
        + df2_ca["Siège (en TJ)"]
        + df2_ca["Parquet général"]
        + df2_ca["Parquet (placés)"]
        + df2_ca["Parquet (en TJ)"],
    )
    cols_tj = ["Siège", "Parquet", "JE", "JI", "JAP", "JCP", "JLD", "Magistrats"]
    cols_ca = [
        "Siège (CA juridiction)",
        "Siège (placés)",
        "Siège (en TJ)",
        "Parquet général",
        "Parquet (placés)",
        "Parquet (en TJ)",
        "JE",
        "JI",
        "JAP",
        "JCP",
        "JLD",
        "Magistrats",
    ]

    for col in cols_tj:
        df2_tj["evol_" + col + "_" + an1] = df2_tj[col] - df1_tj[col]
        df2_tj["augm_" + col + "_" + an1] = np.where(
            df2_tj["evol_" + col + "_" + an1] <= 0, 0, df2_tj["evol_" + col + "_" + an1]
        )
        augmentation_totale = df2_tj["augm_" + col + "_" + an1].sum()
        df2_tj["augm_" + col + "_" + an1 + "_part"] = (
            df2_tj["augm_" + col + "_" + an1] / augmentation_totale
        )

    for col in cols_ca:
        df2_ca["evol_" + col + "_" + an1] = df2_ca[col] - df1_ca[col]
        df2_ca["augm_" + col + "_" + an1] = np.where(
            df2_ca["evol_" + col + "_" + an1] <= 0, 0, df2_ca["evol_" + col + "_" + an1]
        )
        augmentation_totale = df2_ca["augm_" + col + "_" + an1].sum()
        df2_ca["augm_" + col + "_" + an1 + "_part"] = (
            df2_ca["augm_" + col + "_" + an1] / augmentation_totale
        )

    return [df2_tj, df2_ca]
