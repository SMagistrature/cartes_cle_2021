import os

import geopandas as gpd
import pandas as pd


def perimetres(an):
    ressorts = pd.read_csv(os.path.join("sources", an, "ressorts" + an + ".csv"))
    f = gpd.read_file(os.path.join("sources", an, "donnees_IGN_" + an, "COMMUNE.shp"))

    if not len(f) == len(ressorts):
        print(
            f"Attention, il y a un écart de {abs(len(f)-len(ressorts))} communes entre les données IGN et la liste des ressorts ; cela concerne les communes suivantes :"
        )
        liste_ressorts = ressorts["code"].to_list()
        liste_communes = f["INSEE_COM"].to_list()

        to_remove = []

        k = 0
        for commune in liste_communes:
            if commune not in liste_ressorts:
                k += 1
                if k == 1:
                    print(
                        "Communes présentes dans la liste INSEE et manquantes dans le fichier ressorts :"
                    )
                c = f.loc[f.INSEE_COM == commune]
                print(f"{c.INSEE_COM[0]} ({c.NOM[0]})")

        for commune in liste_ressorts:
            if commune not in liste_communes:
                k += 1
                if k == 1:
                    print(
                        "Communes présentes dans le fichier ressorts et manquantes dans la liste INSEE :"
                    )
                print(commune)
                to_remove.append(commune)

        update_ressorts = str(
            input(
                "\nIl est probable que cela soit lié à des fusions de communes ; souhaitez-vous actualiser le fichier ressorts.csv ?  [o/N]   "
            )
        )

        if update_ressorts == "o":
            for commune in to_remove:
                ressorts = ressorts[ressorts.code != commune]
            ressorts.to_csv(
                os.path.join("sources", an, "ressorts" + an + ".csv"), index=False
            )

    f = f.drop(
        columns=[
            "ID",
            "NOM",
            "NOM_M",
            "STATUT",
            "INSEE_ARR",
            "INSEE_DEP",
            "INSEE_CAN",
            "INSEE_REG",
            "SIREN_EPCI",
        ]
    )
    f = f.rename(columns={"INSEE_COM": "code"})
    f = f.merge(ressorts, on="code")
    f = f.dissolve(by="TJ", aggfunc={"POPULATION": "sum", "TPE": "first"})
    f = f.reset_index()
    for ville in [
        "BASSE TERRE",
        "CAYENNE",
        "MAMOUDZOU",
        "FORT DE FRANCE",
        "POINTE A PITRE",
        "ST DENIS",
        "ST PIERRE",
    ]:
        f = f[f["TJ"] != ville]
    f.to_file(
        os.path.join(
            "..", "pages", "public", "resultats", an, "cartes", "carte_tj.geojson"
        ),
        driver="GeoJSON",
    )

    cours = pd.read_csv(
        os.path.join("sources", an, "cle" + an, "TJ_siege.csv"), delimiter=","
    )[["TJ", "COUR"]]
    g = f.merge(cours, on="TJ").fillna(0)
    g = g.drop(columns=["TJ"])
    g = g.dissolve(by="COUR", aggfunc="sum")
    g = g.reset_index()
    g.to_file(
        os.path.join(
            "..", "pages", "public", "resultats", an, "cartes", "carte_ca.geojson"
        ),
        driver="GeoJSON",
    )
