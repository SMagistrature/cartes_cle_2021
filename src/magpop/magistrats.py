from os.path import join as j

import geopandas as gpd
import pandas as pd

public_url = j("..", "pages", "public")


def localisation(write=False, an="2023"):
    cle_siege = pd.read_csv(
        j("sources", an, "cle" + an, "TJ_siege.csv"), delimiter=","
    ).fillna(0)
    cle_siege_ca = cle_siege.drop(columns=["TJ"]).groupby("COUR").sum().reset_index()
    cle_parquet = pd.read_csv(
        j("sources", an, "cle" + an, "TJ_parquet.csv"), delimiter=","
    ).fillna(0)
    cle_parquet_ca = (
        cle_parquet.drop(columns=["TJ"]).groupby("COUR").sum().reset_index()
    )
    cle_CA = pd.read_csv(j("sources", an, "cle" + an, "CA.csv"), delimiter=",").fillna(
        0
    )
    cle_places = pd.read_csv(
        j("sources", an, "cle" + an, "placés.csv"), delimiter=","
    ).fillna(0)
    carte_tj = gpd.read_file(
        j(public_url, "resultats", an, "cartes", "carte_tj.geojson")
    )
    carte_ca = gpd.read_file(
        j(public_url, "resultats", an, "cartes", "carte_ca.geojson")
    )

    tj = pd.DataFrame(
        {
            "TJ": cle_siege["TJ"].tolist(),
            "Siège": cle_siege["TOTAL"].tolist(),
            "Parquet": cle_parquet["TOTAL"].tolist(),
            "JE": cle_siege.loc[:, ["JEHH", "JEBBIS", "VPE", "JE"]]
            .sum(axis=1)
            .tolist(),
            "JI": cle_siege.loc[:, ["JIHH", "JIBBIS", "VPI", "JI"]]
            .sum(axis=1)
            .tolist(),
            "JAP": cle_siege.loc[:, ["JAPHH", "JAPBBIS", "VPAP", "JAP"]]
            .sum(axis=1)
            .tolist(),
            "JCP": cle_siege.loc[:, ["JCPHH", "JCPBBIS", "VPCP", "JCP", "JLF"]]
            .sum(axis=1)
            .tolist(),
            "JLD": cle_siege.loc[:, ["JLDHH", "JLDBBIS", "VPLD"]].sum(axis=1).tolist(),
        }
    )

    ca = pd.DataFrame(
        {
            "COUR": cle_CA["COUR"].tolist(),
            "Siège (CA juridiction)": cle_CA["Total siège"],
            "Siège (placés)": cle_places["Total siège"],
            "Siège (en TJ)": cle_siege_ca["TOTAL"],
            "Parquet général": cle_CA["Total parquet"],
            "Parquet (placés)": cle_places["Total parquet"],
            "Parquet (en TJ)": cle_parquet_ca["TOTAL"],
            "JE": cle_siege_ca.loc[:, ["JEHH", "JEBBIS", "VPE", "JE"]]
            .sum(axis=1)
            .tolist(),
            "JI": cle_siege_ca.loc[:, ["JIHH", "JIBBIS", "VPI", "JI"]]
            .sum(axis=1)
            .tolist(),
            "JAP": cle_siege_ca.loc[:, ["JAPHH", "JAPBBIS", "VPAP", "JAP"]]
            .sum(axis=1)
            .tolist(),
            "JCP": cle_siege_ca.loc[:, ["JCPHH", "JCPBBIS", "VPCP", "JCP", "JLF"]]
            .sum(axis=1)
            .tolist(),
            "JLD": cle_siege_ca.loc[:, ["JLDHH", "JLDBBIS", "VPLD"]]
            .sum(axis=1)
            .tolist(),
        }
    )

    carte_tj = carte_tj.merge(tj, on="TJ")
    carte_ca = carte_ca.merge(ca, on="COUR")

    if write:
        with pd.ExcelWriter(j(public_url, "resultats", an, "synthese.xlsx")) as writer:
            carte_ca.drop(columns=["geometry", "TPE"]).to_excel(
                writer, sheet_name="Cours d'appel", index=False
            )
            carte_tj.drop(columns=["geometry"]).to_excel(
                writer, sheet_name="Tribunaux judiciaires", index=False
            )
        with pd.ExcelWriter(
            j(public_url, "resultats", an, "synthese_cours.xlsx")
        ) as writer:
            carte_ca.drop(columns=["geometry", "TPE"]).to_excel(
                writer, sheet_name="Cours d'appel", index=False
            )
        with pd.ExcelWriter(
            j(public_url, "resultats", an, "synthese_tribunaux.xlsx")
        ) as writer:
            carte_tj.drop(columns=["geometry"]).to_excel(
                writer, sheet_name="Tribunaux judiciaires", index=False
            )
    return [carte_tj, carte_ca]


def ecrire(an):
    localisation(write=True, an=an)
