+++
date = '{{ .Date }}'
draft = true
title = '{{ replace .File.ContentBaseName "-" " " | title }}'
[params]
  legifrance = ''
  decret = 'décret n° du'
  cle = ''
+++
