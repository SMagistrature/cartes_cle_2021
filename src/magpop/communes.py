import os
import shutil

import py7zr
import requests

liens = {
    "2021": [
        "https://data.geopf.fr/telechargement/download/ADMIN-EXPRESS-COG-CARTO/ADMIN-EXPRESS-COG-CARTO_3-0__SHP_WGS84G_FRA_2021-05-19/ADMIN-EXPRESS-COG-CARTO_3-0__SHP_WGS84G_FRA_2021-05-19.7z",
        "ADMIN-EXPRESS-COG-CARTO_3-0__SHP_WGS84G_FRA_2021-05-19",
    ],
    "2022": [
        "https://data.geopf.fr/telechargement/download/ADMIN-EXPRESS-COG-CARTO/ADMIN-EXPRESS-COG-CARTO_3-1__SHP_WGS84G_FRA_2022-04-15/ADMIN-EXPRESS-COG-CARTO_3-1__SHP_WGS84G_FRA_2022-04-15.7z",
        "ADMIN-EXPRESS-COG-CARTO_3-1__SHP_WGS84G_FRA_2022-04-15",
    ],
    "2023": [
        "https://data.geopf.fr/telechargement/download/ADMIN-EXPRESS-COG-CARTO/ADMIN-EXPRESS-COG-CARTO_3-2__SHP_WGS84G_FRA_2023-05-03/ADMIN-EXPRESS-COG-CARTO_3-2__SHP_WGS84G_FRA_2023-05-03.7z",
        "ADMIN-EXPRESS-COG-CARTO_3-2__SHP_WGS84G_FRA_2023-05-03",
    ],
    "2024": [
        "https://data.geopf.fr/telechargement/download/ADMIN-EXPRESS-COG-CARTO/ADMIN-EXPRESS-COG-CARTO_3-2__SHP_WGS84G_FRA_2024-02-22/ADMIN-EXPRESS-COG-CARTO_3-2__SHP_WGS84G_FRA_2024-02-22.7z",
        "ADMIN-EXPRESS-COG-CARTO_3-2__SHP_WGS84G_FRA_2024-02-22",
    ],
}


def import_ign(force=False, an="2024"):
    if not os.path.isfile("ign.7z") or force:
        with requests.get(liens[an][0], stream=True) as r:
            r.raise_for_status()
            with open("ign.7z", "wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)


def extract_ign(an):
    if not os.path.isdir(os.path.join("sources", an, f"donnees_IGN_{an}")):
        with py7zr.SevenZipFile("ign.7z", "r") as archive:
            allfiles = archive.getnames()
            selection = [f for f in allfiles if "COMMUNE." in f]
            archive.extract(targets=selection, path=os.path.join("sources", an))
        os.rename(
            os.path.join("sources", an, liens[an][1]),
            os.path.join("sources", an, f"donnees_IGN_{an}"),
        )
        for p, d, f in os.walk(os.path.join("sources", an, f"donnees_IGN_{an}")):
            if f != []:
                for fichier in f:
                    os.rename(
                        os.path.join(p, fichier),
                        os.path.join("sources", an, f"donnees_IGN_{an}", fichier),
                    )


def suppression(an):
    if os.path.isdir(os.path.join("sources", an, "donnees_IGN")):
        shutil.rmtree(os.path.join("sources", an, "donnees_IGN"))
    os.remove("ign.7z")
