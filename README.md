# Carte judiciaire et population

Population du ressort des tribunaux judiciaires & cours d'appels de France métropolitaine, croisée avec le nombre d'emplois de magistrats "localisés" (selon la circulaire de localisation des emplois).

## Sources

Ces données sont obtenues à partir des sources ci-dessous et sont ensuite traitées en utilisant principalement la bibliothèque python [geopandas](https://geopandas.org)

Les sources de données sont les suivantes :
- carte des communes : [Base Admin Express COG Carto (IGN+INSEE)](https://geoservices.ign.fr/adminexpress#telechargementCogCarto)
- populations légales : [INSEE](https://www.insee.fr) et [legifrance](https://www.legifrance.gouv.fr)
- Ressort des tribunaux judiciaires : [décret](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000047983476), [données Min.Just.](https://www.observatoire-des-territoires.gouv.fr/index.php/perimetre-des-ressorts-des-tribunaux-judiciaires)
- circulaires de localisation des emplois (CLE): versions communiquées aux organisations syndicales

## Tableaux

Les résultats sous forme de fichiers excel déjà produits par cet outil sont détaillés et synthétisés dans le dossier [**resultats**](https://gitlab.com/SMagistrature/magistrats_par_habitant/tree/main/pages/public/resultats) ainsi qu'à l'adresse [https://www.data.gouv.fr/fr/datasets/effectifs-de-magistrats-par-habitant/](https://www.data.gouv.fr/fr/datasets/effectifs-de-magistrats-par-habitant/).

## Cartes

Des cartes interactives permettant de visualiser ces données sont, par ailleurs, accessibles en ligne :

### 1. Population par ressort
- [Nombre d'habitants par ressort de cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_population.html)
- [Nombre d'habitants par ressort de tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_population.html)
- [Evolution de la population depuis 2021 (ressort des TJ)](https://cartejudiciaire.fr/resultats/2024/cartes/tj_population_evol.html)
- [Evolution de la population depuis 2021 (ressort des CA)](https://cartejudiciaire.fr/resultats/2024/cartes/ca_population_evol.html)

### 2. Nombre d'emplois de magistrats localisés
- [Nombre de magistrats par ressort de cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_cle.html)
- [Nombre de magistrats par ressort de tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_cle.html)
- [Evolution de la CLE depuis 2021 (ressort des TJ)](https://cartejudiciaire.fr/resultats/2024/cartes/tj_cle_evol.html)
- [Evolution de la CLE depuis 2021 (ressort des CA)](https://cartejudiciaire.fr/resultats/2024/cartes/ca_cle_evol.html)

### 3. Nombre d'emplois de magistrats rapporté au nombre d'habitants

#### a. Ressort des cours d'appel
- [Ensemble des magistrats](https://cartejudiciaire.fr/resultats/2024/cartes/ca_ensemble.html)
- [Ensemble des magistrats du siège, avec détail par fonction spécialisée](https://cartejudiciaire.fr/resultats/2024/cartes/ca_siege.html)
- [Ensemble des magistrats du parquet](https://cartejudiciaire.fr/resultats/2024/cartes/ca_parquet.html)

#### b. Ressort des tribunaux judiciaires :
- [Ensemble des magistrats](https://cartejudiciaire.fr/resultats/2024/cartes/tj_ensemble.html)
- [Juges du siège (en TJ), avec détail par fonction spécialisée](https://cartejudiciaire.fr/resultats/2024/cartes/tj_siege.html)
- [Parquetiers](https://cartejudiciaire.fr/resultats/2024/cartes/tj_parquet.html)

#### c. Cartes par fonction spécialisée
- JLD : visualisation [par cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_JLD.html) ou [par tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_JLD.html)
- JCP : visualisation [par cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_JCP.html) ou [par tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_JCP.html)
- JE : visualisation [par cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_JE.html) ou [par tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_JE.html)
- JI : visualisation[par cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_JI.html) ou [par tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_JI.html)
- JAP : visualisation [par cour d'appel](https://cartejudiciaire.fr/resultats/2024/cartes/ca_JAP.html) ou [par tribunal judiciaire](https://cartejudiciaire.fr/resultats/2024/cartes/tj_JAP.html)